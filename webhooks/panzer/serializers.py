from rest_framework import serializers
from .models import *


class RepositorySerializer(serializers.ModelSerializer):
    """ Serialiazer de HookSend. """

    name = serializers.CharField(max_length=50)
    data = serializers.CharField(max_length=3000,read_only=True)
    
    def create(self, validated_data):
    	#in_data = validated_data.pop('in_data',[])
    	return Repository.objects.create(**validated_data)

    class Meta:
        model = Repository
        fields = ('id','in_data','name','status','data')