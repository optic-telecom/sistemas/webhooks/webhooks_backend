from django.contrib import admin
from utils.admin import BaseAdmin

from .models import *


@admin.register(Repository)
class RepositoryAdmin(BaseAdmin):
    list_display = ('name','data')