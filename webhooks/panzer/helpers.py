import json
import redis

def get_data_redis(key):
    try:
        r = get_conection()
        d = r.lrange(key, 0, -1)
        if d:
            return d
        return None
    except Exception as e:
        print ('error')
        print (e)
        return None 


def get_conection():
    return redis.Redis(host='localhost', port=6379, db=0)