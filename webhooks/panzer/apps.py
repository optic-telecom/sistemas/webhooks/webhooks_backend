from django.apps import AppConfig


class PanzerConfig(AppConfig):
    name = 'panzer'
