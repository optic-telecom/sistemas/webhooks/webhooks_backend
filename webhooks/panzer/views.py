import requests

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action

from utils.permissions import IsSystemInternalPermission

from .serializers import RepositorySerializer
from .models import *


class RepositoryViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Repository
    retrieve:
        End point for Repository
    update:
        End point for Repository
    delete:
        End point for Repository
    """
    queryset = Repository.objects.all()
    serializer_class = RepositorySerializer
    permission_classes = [IsSystemInternalPermission|IsAuthenticated]
    lookup_field = 'name'

    def to_json(self, rows):
        result = []
        for x in rows:
            try:
                result.append(json.loads(json.loads(x)))
            except TypeError as e:
                result.append(json.loads(x))
            
        return result

    def to_json2(self, rows):
        result = []
        for x in rows:
            try:
                result.append(json.loads(x))
            except Exception as e:
                print (x)
                result.append(x)
        return result


    @action(detail=True, methods=['get'])
    def from_redis(self, request, version, name):
        from .helpers import get_data_redis
        data = get_data_redis(name)
        if data:
            response_data = self.to_json(data)
        else:
            try:
                data = Repository.objects.get(name=name)
                response_data = self.to_json2(data.data)
            except Repository.DoesNotExist:
                response_data = None
            
        return Response({'result':response_data})
