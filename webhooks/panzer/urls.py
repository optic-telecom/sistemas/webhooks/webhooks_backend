from django.urls import path, re_path
from rest_framework import routers
from .views import RepositoryViewSet

router = routers.DefaultRouter()
router.register(r'repository', RepositoryViewSet, 'repository_api')
