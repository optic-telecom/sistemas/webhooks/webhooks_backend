import redis
import json
from django.db import models
from utils.models import BaseModel
from django.contrib.postgres.fields import JSONField, ArrayField
from django.contrib.auth import get_user_model
from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver
from .helpers import get_conection


User = get_user_model()


class Repository(BaseModel):
    in_data   = JSONField()
    data      = ArrayField(models.CharField(max_length=200))
    name      = models.CharField(max_length=60, unique=True)
    status    = models.BooleanField(default = True)

    def __str__(self):
        return f"{self.status}"


    class Meta:
        ordering = ['-id']
        #unique_together = ['model', 'action']



@receiver(pre_save, sender=Repository)
def RepositoryPostSave(sender, **kwargs):
    #print (kwargs)

    redis_conection = get_conection()
    i = kwargs['instance']
    _lista = []
    try:
        _l = redis_conection.lrange(i.name, 0, -1)
        # for item in _l:
        #     print (item)
        #     _lista.append(json.dumps(item))
        _lista = _l
    except Exception as e:
        pass

    #print (json.loads(_lista[3]))
    _lista.append(i.in_data)

    #r.set(i.name, i.data)
    redis_conection.lpush(i.name, json.dumps(i.in_data))
    #print (_lista)
    
    #print (_lista)
    i.data = None
    i.data = _lista



@receiver(pre_delete, sender=Repository)
def delete_repo(sender, instance, **kwargs):
    redis_conection = get_conection()
    redis_conection.delete(instance.name)