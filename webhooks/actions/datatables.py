import json
import requests
from typing import Any, Dict, List

from django.db.models.query import QuerySet
from utils.datatables import BaseDatatables
from .models import Hook, HookSend


class GenericDatatable(BaseDatatables):

    FIELDS_FILTERS: Dict[str, str] = {
        "created": "datetime",
        "hook__pk": "str",
        "data_in": "str",
    }

    FIELDS_SORTABLE = ["created", "id"]

    model = HookSend

    def modify_sortable(self):

        # Buscamos el Hook consultado para buscar sus columnas
        title = self.request.data.get('title', None)
        current_hook_columns = Hook.objects.get(title=title).data_columns
        current_hook_columns = current_hook_columns.split(',')

        for column in current_hook_columns:
            self.FIELDS_SORTABLE += f'{column.strip()}'

    def get_filtered_search(self, qs: QuerySet) -> QuerySet:
        """

            This function, search queryset

            :returns: Filtered querySet with search field

        """

        search: str = self.SEARCH

        # qs: QuerySet = qs.filter( Q(name__icontains=search) | Q(template__icontains=search) | Q(creator__username__icontains=search))
        if search:

            if search.isdigit():

                qs: QuerySet = qs.filter(id=int(search))

            else:

                wanted_items = set()

                for item in qs:
                    data = item.data_post_in.replace("\'", "\"")
                    data = json.loads(data)

                    if search in data.get("Nombre") or search in data.get(
                            "Direccion") or search in data.get("Rut"):
                        wanted_items.add(item.id)

                qs = qs.filter(id__in=wanted_items)

        return qs

    def get_instance_to_dict(self, instance) -> Dict[str, Any]:
        """

            This function, return dict of instance

        """

        # Comprobamos la información tanto en el data_get_in como en el
        # data_post_in del modelo para obtener los datos
        if instance.data_post_in != '':
            data = instance.data_post_in.replace("\'", "\"")
            data = data.replace('True', '"true"')
        elif instance.data_get_in != '':
            data = instance.data_get_in.replace("\'", "\"")
            data = data.replace('True', '"true"')
        elif instance.data_json_in.strip() != '':
            data = instance.data_json_in.replace("\'", "\"")
            data = data.replace('True', '"true"')

        # Existe conflicto con algunos datos en los hooks, por eso esta sección
        try:
            # parseamos el objeto de json a un diccionario en Python
            data = json.loads(data)
        except Exception as e:
            pos_to_replace = e.args[0].split(" ")[6]
            # print(pos_to_replace)
            data = list(data)
            data[int(pos_to_replace)] = "'"
            data = "".join(data)
            data = json.loads(data)

        # Estos dos atributos estarán en todos los registros
        final_data = {}
        final_data['id'] = instance.id
        final_data['created'] = instance.created

        # Creamos un diccionario para almacenar las direcciones y así
        # Optimizar el número de peticiones a pulso
        direcciones = {}

        # Necesitamos mandar los campos así estén vacíos, entonces
        # accedemos a ellos a través del hook asociado a cada hooksend
        columns = instance.hook.data_columns.split(',')
        for column in columns:
            # Si la columna existe en la data actual, se agrega a la data final
            if f'{column.strip()}' in data:

                column_value = data[f'{column.strip()}']

                # Si la columna es complete_location o street_location
                # Buscamos en pulso y almacenamos en un dict para optimizar
                # las peticiones hechas
                if column.strip() == 'street_location' \
                        and column_value != '':

                    # Si el número de dirección actual no está en el dict
                    # pues buscamos la direccion en pulso y la almacenamos
                    if column_value not in direcciones:
                        direccion = self.search_in_pulso(
                            street_location=column_value
                        )
                        
                        direcciones[f'{column_value}'] = direccion
                        final_data[f'{column.strip()}'] = direcciones[f'{column_value}']
                    # Si el número de direccion actual está en el dict
                    # lo obtenemos
                    else:
                        final_data[f'{column.strip()}'] = direcciones[f'{column_value}']
                elif column.strip() == 'complete_location' \
                        and column_value != '':
                    # Si el número de dirección actual no está en el dict
                    # pues buscamos la direccion en pulso y la almacenamos
                    if column_value not in direcciones:
                        direccion = self.search_in_pulso(
                            complete_location=column_value
                        )
                        
                        direcciones[f'{column_value}'] = direccion
                        final_data[f'{column.strip()}'] = direcciones[f'{column_value}']
                    # Si el número de direccion actual está en el dict
                    # lo obtenemos
                    else:
                        final_data[f'{column.strip()}'] = direcciones[f'{column_value}']
                else:
                    final_data[f'{column.strip()}'] = column_value
            # Si no existe pues se crea la llave pero con un valor nulo
            else:
                final_data[f'{column.strip()}'] = ''

        return final_data

    def get_initial_queryset(self) -> QuerySet:
        """

            This function, filter queryset by operator

        """
        title = self.request.data.get('title', None)

        # Base QuerySet
        qs = super().get_initial_queryset()
        qs = qs.filter(hook__title=title)
        return qs

    def get_struct(self) -> Dict[str, Any]:
        """

            This function, get datables struct

            :returns: Dict, with datatables definitions

        """

        data_struct: Dict[str, Any] = super().get_struct()

        data_struct["defaults"] = {
            "order_field": "created",
            "order_type": "desc",
            "start": 0,
            "offset": 10,
            "filters": [],
            "search": "",
            "scroll": 900
        }
        # Buscamos el Hook consultado para buscar sus columnas
        title = self.request.query_params.get('title')
        current_hook_columns = Hook.objects.get(title=title).data_columns
        current_hook_columns = current_hook_columns.split(',')

        # asociamos los tipos de campo de manera dinámica
        data_struct["fields"] = {}
        fields = {}
        fields['id'] = 'int'
        fields['created'] = 'datetime'
        for column in current_hook_columns:
            fields[f'{column.strip()}'] = 'str'

        data_struct["fields"] = fields

        # Asociamos las columnas de manera dinámica
        data_struct["columns"] = {}
        cols = {}
        count = 1
        for column in current_hook_columns:
            cols[f'{column.strip()}'] = {
                "field": f"{column.strip()}",
                "sortable": True,
                "visible": True,
                "position": count,
                "width": 100,
                "fixed": None
            }
            count += 1

        cols['Fecha de Creación'] = {
            "field": "created",
            "sortable": True,
            "visible": True,
            "position": 1,
            "width": 100,
            "fixed": None
        }

        data_struct["columns"] = cols

        data_struct["filters"] = {
            "Fecha de inicio": {
                "type": "datetime",
                "name": "created",
                "format": "%d/%m/%Y %H:%M",
            },
            "Fecha de fin": {
                "type": "datetime",
                "name": "created",
                "format": "%d/%m/%Y %H:%M",
            }
        }

        return data_struct

    def search_in_pulso(self, street_location=None, complete_location=None):
        headers = {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}

        if street_location:
            url = 'https://pulso.multifiber.cl/api/v1/search-complete-location/'
            response = requests.get(url=url+f'{street_location}/', headers=headers)
            data = json.loads(response.content)
            direction_string = data[0]['street_location']
            return direction_string

        if complete_location:
            url = 'https://pulso.multifiber.cl/api/v1/complete-location/'
            response = requests.get(url=url+f'{complete_location}/', headers=headers)
            data = json.loads(response.content)
            direction_string = data['street_location'] + ', ' + data['departament']
            return direction_string
