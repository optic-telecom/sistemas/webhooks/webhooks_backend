from rest_framework import serializers
from .models import *


class HookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hook
        fields = ('id', 'title', 'url_in', 'url_out', 'code', 'app',
                  'data_header', 'method', 'status', 'data_columns')

    def create(self, validated_data):
        #json
        url_out_raw = validated_data.get('url_out')
        url_out = [{'url': url_out_raw}]
        method = validated_data.get('method', 'POST').upper()

        nh = Hook.objects.create(method=method,
                                 code=validated_data.get('code'),
                                 status=validated_data.get('status', True),
                                 url_in=validated_data.get('url_in'),
                                 url_out=url_out)
        return nh

    def is_valid(self, data, *args, **kwargs):
        """ Valida que el modelo no se haya creado ya. """

        func = data.pop('code')
        if 'dispach_hooks' not in func:
            _err = {
                'error': 'La función se debe llamar exactamente dispach_hooks'
            }
            raise serializers.ValidationError(_err)
        return super(HookSerializer, self).is_valid(*args, **kwargs)


class HookSendSerializer(serializers.ModelSerializer):
    """ Serialiazer de HookSend. """
    class Meta:
        model = HookSend
        fields = ('id', 'data_get_in', 'data_post_in', 'data_out')