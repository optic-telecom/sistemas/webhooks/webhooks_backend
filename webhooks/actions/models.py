from django.db import models
from utils.models import BaseModel
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.postgres.fields import JSONField
from django.contrib.auth import get_user_model

User = get_user_model()


class Hook(BaseModel):
    """ """
    url_in = models.CharField(max_length=50, unique=True)
    url_out = JSONField(null=True, blank=True)  #Esto podra ser una lista
    title = models.CharField(max_length=50,
                             unique=False,
                             null=True,
                             blank=True)
    code = models.TextField(null=True, blank=True)
    file = models.CharField(max_length=150,
                            unique=False,
                            null=True,
                            blank=True)
    app = models.CharField(max_length=50, unique=False, null=True, blank=True)
    data_header = JSONField(null=True, blank=True)
    method = models.CharField(max_length=10, unique=False, default='POST')
    status = models.BooleanField(default=True)
    send_out = models.BooleanField(default=True)
    data_columns = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.url_in}_{self.title}"

    def to_panzer(self):
        return {'title': self.title}

    class Meta:
        ordering = ['-id']
        #unique_together = ['model', 'action']


class HookSend(BaseModel):
    """ Información que se recibio y guardo antes de disparar el hook"""
    hook = models.ForeignKey(Hook, on_delete=models.CASCADE)
    data_get_in = models.TextField(null=True, blank=True)
    data_post_in = models.TextField(null=True, blank=True)
    data_json_in = models.TextField(null=True, blank=True)
    data_out = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ['-hook']

    def __str__(self):
        return str(self.hook)