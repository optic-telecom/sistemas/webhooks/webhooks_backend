from __future__ import absolute_import, unicode_literals
from tasks.celery import app as celery_app
import requests
from django.conf import settings

from .models import Action, System, WebHook, WebHookTransaction, TransactionReplication,\
    PROCESSED, ERROR

def get_user_token(url):
    """ Obtiene el token de autorización para el header """
    data = {
        'username': settings.TASK_USERNAME,
        'password': settings.TASK_PASSWORD
    }
    protocol = 'http://'
    if protocol not in url:
        url = protocol + url
    auth_response = requests.post(url, data = data)
    token = auth_response.json()['token']
    headers={'Authorization': 'jwt '+token}
    return headers

def send_request(url, action, headers, data):
    """ Envía un request a la url con data. """
    # Send request based on action
    if action=='create':
        response = requests.post(url, data = data, headers=headers)
    elif action=='update':
        # id en la data para el url
        url = url + data['id'] + '/'
        response = requests.patch(url, data = data, headers=headers)
    elif action=='delete':
        url = url + data['id'] + '/'
        # header['replicate'] = True # Header para evitar la replica
        response = requests.delete(url, data = data, headers=headers)
    # Return status code
    return response.status_code