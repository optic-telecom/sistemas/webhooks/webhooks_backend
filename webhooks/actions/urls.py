from django.urls import path, re_path
from rest_framework import routers
from .views import HookViewSet, HookSendViewSet

router = routers.DefaultRouter()
router.register(r'hooks', HookViewSet, 'hook_api')
router.register(r'hooks_send', HookSendViewSet, 'hook_send_api')
