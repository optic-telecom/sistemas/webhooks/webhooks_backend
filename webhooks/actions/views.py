import requests
import json
import redis
import os
from rq import Queue, Worker, Retry
from rq import get_current_job
from rest_framework.decorators import action

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.development')
import django

django.setup()

from importlib import reload  

from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
#from .rrhhMultifiber import Multifiber_settings

from utils.permissions import IsSystemInternalPermission
from .datatables import GenericDatatable

from .serializers import HookSerializer, HookSendSerializer
from .models import *

import logging

logger = logging.getLogger('logger')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(pathname)s at line %(lineno)d: %(message)s in function %(funcName)s')
file_handler = logging.FileHandler('debuglogging.log')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

# TODO I havent imported the redis password, you can find it in hooks-> Multifiber RRHH -> settings
redis_conn = redis.Redis(
    host="127.0.0.1",
    port="6379")  #, password=Multifiber_settings.REDIS_PASSWORD)

redis_queue = Queue(connection=redis_conn)

#worker = Worker(redis_queue, connection=redis_conn)


class RequestClone:
    """
    Clase que imita el objeto WSGIRequest de Django para poder ser
    serializado de manera asíncrona con el paquete rq
    """
    def __init__(self, request):
        self.POST = self.POST(request.POST)
        self.GET = self.GET(request.GET)

    # Inner Class Get
    class GET:
        def __init__(self, body):
            self.body = body.dict()

        def dict(self):
            return self.body

        def get(self, value, default=None):
            try:
                dict_value = self.body[value]
                return dict_value
            except Exception as e:
                return default

    # Inner Class Post
    class POST:
        def __init__(self, body):
            self.body = body.dict()

        def dict(self):
            return self.body

        def get(self, value, default=None):
            try:
                dict_value = self.body[value]
                return dict_value
            except Exception as e:
                return default


@csrf_exempt
def send_dispach(request, url_in):
    hook = Hook.objects.get(url_in=url_in)

    try:
        json_dict = json.loads(request.body.decode('utf-8'))
    except Exception as e:
        json_dict = {}

    IN_GET = request.GET.dict()
    IN_POST = request.POST.dict()

    if hook.file:
        import importlib
        imp = hook.file.split('.')
        imp_module = ".".join(imp[:-1])
        sender = importlib.import_module(imp_module)
        reload(sender)
        function_hook = getattr(sender, imp[-1])
        if callable(function_hook):
            try:
                if isinstance(function_hook, type):
                    instance = function_hook()
                    res = instance(request, hook)
                else:
                    res = function_hook(request, hook)

                HookSend.objects.create(hook=hook,
                                        data_get_in=IN_GET,
                                        data_post_in=IN_POST,
                                        data_json_in=json_dict)

                if isinstance(res, (JsonResponse, HttpResponse)):
                    return res

                return JsonResponse({'success': True}, safe=False)

            except Exception as e:
                if settings.DEBUG:
                    raise e

                return JsonResponse({
                    'success': False,
                    'error': str(e)
                },
                                    safe=False)

        return JsonResponse({'success': True}, safe=False)

    elif hook.send_out:
        url = hook.url_out[0]['url']
        headers = hook.data_header if hook.data_header else None
        code_objeto = compile(hook.code, 'file_python_code', 'exec')
        exec(code_objeto)
        data_return = locals()['dispach_hooks'](request, locals())
        if hook.method == 'POST':
            req = requests.post(url, json=data_return, headers=headers)
        else:
            req = requests.get(url, json=data_return, headers=headers)
        HookSend.objects.create(hook=hook,
                                data_get_in=IN_GET,
                                data_post_in=IN_POST,
                                data_json_in=json_dict,
                                data_out=req.text)
        return JsonResponse(req.text, safe=False)

    else:
        code_objeto = compile(hook.code, 'file_python_code', 'exec')
        exec(code_objeto)
        HookSend.objects.create(hook=hook,
                                data_get_in=IN_GET,
                                data_json_in=json_dict,
                                data_post_in=IN_POST)

        return JsonResponse({'success': True}, safe=False)


@csrf_exempt
def send_dispach_async(request, url_in):
    try:
        new_req = RequestClone(request)
        job = redis_queue.enqueue(send_dispach,
                                  new_req,
                                  url_in,
                                  retry=Retry(max=1))
        return JsonResponse({
            'success': True,
            'Id': str(job.id),
            'status': str(job.get_status())
        })
    except Exception as e:
        if settings.DEBUG:
            raise e
        return JsonResponse({'success': False, 'error': str(e)}, safe=False)


class HookViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Hook
    retrieve:
        End point for Hook
    update:
        End point for Hook
    delete:
        End point for Hook
    """
    lookup_field = 'url_in'
    queryset = Hook.objects.all()
    serializer_class = HookSerializer


class HookSendViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for HookSend
    retrieve:
        End point for HookSend
    update:
        End point for HookSend
    delete:
        End point for HookSend
    """
    queryset = HookSend.objects.all()
    serializer_class = HookSendSerializer

    @action(detail=False, methods=['get'], url_path='datatables_struct')
    def datatables_struct(self, request, version) -> JsonResponse:
        """

            This funtion return datatables data

            :param self: Instance of Class TemplateTextView
            :type self: TemplateTextView
            :param request: data of request
            :type request: request

        """

        # from pdb import set_trace
        # set_trace()

        return JsonResponse(GenericDatatable(request).get_struct(), safe=True)

    @action(detail=False, methods=['post'], url_path='datatables_content')
    def datatables_content(self, request, version) -> JsonResponse:
        GenericDatatable(request).modify_sortable()
        return GenericDatatable(request).get_data()
