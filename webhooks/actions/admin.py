from django.contrib import admin
from utils.admin import BaseAdmin

from .models import *

# from dynamic_preferences.registries import global_preferences_registry
# global_preferences = global_preferences_registry.manager()


def copy_hook(modeladmin, request, queryset):
    for obj in queryset:
        obj.pk = None
        obj.url_in = obj.url_in + '_2'
        obj.save()

copy_hook.short_description = "Duplicar hook"


@admin.register(Hook)
class HookAdmin(BaseAdmin):
    list_display = ('id','full_url','url_out','title','data_header','method')
    actions = BaseAdmin.actions + [copy_hook,]

    def full_url(self, obj):
    	proto = 'https://' if self.request.is_secure() else 'http://'
    	return proto + self.request.META['HTTP_HOST'] + '/send/' + obj.url_in

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        self.request = request
        return qs


@admin.register(HookSend)
class HookSendAdmin(BaseAdmin):
    list_display = ('id','hook','data_get_in','data_post_in')
    actions = BaseAdmin.actions + [copy_hook,]