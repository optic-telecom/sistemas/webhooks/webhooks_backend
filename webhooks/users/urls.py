from django.urls import path, re_path
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from .views import UserViewSet, UserView, PasswordReset

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    # url(r'^api/user', UserView.as_view(), name='user_view'),    
    # url(r'^api/password_reset', PasswordReset.as_view()),
    re_path(r'^api/user', UserView.as_view(), name='user_view'),
    re_path(r'^api/password_reset', PasswordReset.as_view()),
    #jwt
    re_path(r'^api/(?P<version>[-\w]+)/auth/obtain_token/', obtain_jwt_token),
    re_path(r'^api/(?P<version>[-\w]+)/auth/refresh_token/', refresh_jwt_token),
    re_path(r'^api/(?P<version>[-\w]+)/auth/verify_jwt_token/', verify_jwt_token),
    
]