from .base import *
from os.path import join

DEBUG = True

ENVIROMENT = 'PRO'


# CELERY_BROKER_URL = 'redis://localhost:6379'
# CELERY_RESULT_BACKEND = CELERY_BROKER_URL
# CELERY_BROKER_TRANSPORT = 'redis'

ALLOWED_HOSTS = ['*']

STATIC_URL = '/static/'
STATIC_ROOT = join(BASE_DIR, 'static')
STATICFILES_DIRS = (
    join(BASE_DIR, 'staticfiles'),
)


BASE_DIR_LOG = join(dirname(BASE_DIR), 'logs')

LOGGING = {
    "version": 1,
    "formatters": {"verbose": {"format": "%(levelname)s:%(name)s: %(message)s"}},
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "file": {
            "level": "INFO",
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "filename": join(BASE_DIR_LOG, "debug_django.log"),
        },
    },
    "loggers": {
        "django.request": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": False,
        },
        "*":{
            "handlers": ["console"],
            "level": "DEBUG",
            "propagate": False,        
        }
    },
}
