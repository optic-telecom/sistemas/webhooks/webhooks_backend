from django.contrib import admin
from utils.views import SwaggerSchemaView, home
from django.urls import path, re_path, include
from users.urls import urlpatterns as urlpatterns_user
from users.urls import router as router_users
from panzer.urls import router as panzer_actions
from actions.urls import router as router_actions
from actions.views import send_dispach, send_dispach_async

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_users.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_actions.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(panzer_actions.urls)),
    path('api/docs', SwaggerSchemaView.as_view()),
    re_path(r'^send/(?P<url_in>[-\w]+)/', send_dispach),
    re_path(r'^task/(?P<url_in>[-\w]+)/', send_dispach_async),
] + urlpatterns_user
