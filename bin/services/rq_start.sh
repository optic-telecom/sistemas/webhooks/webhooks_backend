#!/bin/bash
echo "Iniciando servidor RQ as `whoami`"
LOGFILE=/home/webhooks/webhooks_backend/logs/rq.log
source /home/webhooks/webhooks_env/bin/activate
cd /home/webhooks/webhooks_backend/webhooks
exec rq worker --with-scheduler -c rq_settings 2>>$LOGFILE
